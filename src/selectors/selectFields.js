export const selectStoryFields = ({ id, by, url, time, title, descendants, score, kids } = {}) => ({
    id, by, url, time, title, descendants, score, kids
})

export const selectCommentFields = ({ id, time, text, by, kids, deleted } = {}) => ({
    id, time, text, by, kids, deleted
})