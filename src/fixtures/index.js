export const singularStory = {
    by: 'Strahinja Gligovic',
    id: 1,
    time: 1567209822,
    title: 'Execom Frontend Test',
    url: 'https://www.execom.eu/',
};

export const storyIds = [1];