import React, { useEffect, useState } from 'react';
import { Story } from '../components/story';
import { useParams } from "react-router-dom";
import { getStory } from '../services/hackernewsApi';
import { Comment } from '../components/comment';

export const CommentsContainer = () => {
    const { storyId } = useParams();
    const [story, setStory] = useState({});

    useEffect(() => {
        let isMounted = true;
        getStory(storyId).then(data => data && data.url && isMounted && setStory(data));
        return () => isMounted = false;
    }, [storyId]);

    return (
        <>
            <Story commentStory={story} />
            {story.kids ? story.kids.map((commentId) => <Comment key={commentId} commentId={commentId} />) : null}
        </>
    )
}