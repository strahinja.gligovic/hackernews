import React, { useEffect, useState } from 'react';
import { getStoryIds } from '../services/hackernewsApi';
import { Story } from '../components/story';
import { useInfiniteScroll } from '../hooks/useInfiniteScroll';
import '../styles/storiesContainer.css';

export const StoriesContainer = () => {
    const [storyIds, setStoryIds] = useState([]);
    const { count } = useInfiniteScroll();

    useEffect(() => {
        getStoryIds().then(data => setStoryIds(data));
    }, [])


    return (
        <>
            <div className="stories-container" data-test-id="stories-container">
                {storyIds.slice(0, count).map((storyId, index) => <Story key={storyId} storyId={storyId}
                    index={index}></Story>)}
            </div>
        </>
    );
};
