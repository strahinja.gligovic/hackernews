import React from 'react';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import '../styles/navigation.css';

export const Navigation = () => {
    return (
        <Navbar collapseOnSelect expand="lg"  variant="light">
            <Navbar.Brand as={Link} to ="/news">Hacker News</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            {/* <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/new">New</Nav.Link>
                </Nav>
            </Navbar.Collapse> */}
        </Navbar>
    )
}