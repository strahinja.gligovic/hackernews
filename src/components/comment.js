import React, { useEffect, useState } from 'react';
import { getComment } from '../services/hackernewsApi';
import { mapTime } from '../mappers/mapTime';
import PropTypes from 'prop-types';
import '../styles/comment.css';


export const Comment = ({ commentId, depth }) => {
    const [comment, setComment] = useState({});
    const [visible, setVisible] = useState(true);

    useEffect(() => {
        let isMounted = true;
        getComment(commentId).then(data => data && data.by && isMounted && setComment(data))
        return () => isMounted = false;
    }, [commentId]);

    const toggleVisibility = () => {
        setVisible(!visible);
    }

    const nestedComments = comment.kids ?
        comment.kids.map((commentId) => <Comment key={commentId} commentId={commentId} depth={depth + 1} />) : null

    return (
        <div className={'comment ' + (Object.keys(comment).length === 0 ? 'deleted' : '')}
            style={{ marginLeft: `${depth * 1.5}%` }}>
            <div className="detail-row">
                <span>{comment.by}</span>
                <span>{mapTime(comment.time)} ago</span>
                <span className="clickable" onClick={toggleVisibility}>[{visible ? '-' : '+'}]</span>
            </div>
            <div className="comment-content" style={{ display: (visible ? 'block' : 'none') }}>
                <div className="comment-text" dangerouslySetInnerHTML={{ __html: comment.text }} />
                {nestedComments}
            </div>
        </div>
    )
}

Comment.propTypes = {
    commentId: PropTypes.number,
    depth: PropTypes.number
}

Comment.defaultProps = {
    depth: 0
}