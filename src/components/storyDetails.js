import React from 'react';
import { mapTime } from '../mappers/mapTime';
import { Link } from 'react-router-dom'

export class StoryDetails extends React.Component {
    render() {
        let comments;
        if (this.props.commentMode) {
            const commentsUrl = `/comments/${this.props.story.id}`;
            comments = <Link to={commentsUrl}>{this.props.story.descendants} comments</Link>;
        }
        return (
            <>
                <span>{this.props.story.score} points</span>
                <span data-testid="test-story-by">by {this.props.story.by}</span>
                <span>{mapTime(this.props.story.time)} ago</span>
                {comments}
            </>
        );
    }
}