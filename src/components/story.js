import React, { useEffect, useState } from 'react';
import { getStory } from '../services/hackernewsApi';
import { StoryDetails } from './storyDetails';
import PropTypes from 'prop-types';
import '../styles/story.css';

export const Story = ({ storyId, index, commentStory }) => {
    const [story, setStory] = useState({})
    const commentMode = !!(commentStory === undefined);

    useEffect(() => {
        let isMounted = true;
        if (commentMode) {
            getStory(storyId).then(data => data && data.id && isMounted && setStory(data));
        } else {
            setStory(commentStory);
        }
        return () => isMounted = false;
    }, [storyId, commentStory, commentMode]);

    const indexDisplay = commentMode ? <span>{index + 1}.&nbsp;</span> : null
    return story && story.url ?
        <div data-testid="story" className="story">
            <div className="story-title">
                {indexDisplay}
                <a href={story.url} target="_blank" rel="noopener noreferrer"><span>{story.title}</span></a>
            </div>
            <div className="detail-row">
                <StoryDetails story={story} commentMode={commentMode} />
            </div>
        </div> : null
}

Story.propTypes = {
    storyId: PropTypes.number,
    index: PropTypes.number,
    commentStory: PropTypes.object
}

Story.defaultProps = {
    storyId: undefined,
    index: undefined,
    commentStory: undefined
}