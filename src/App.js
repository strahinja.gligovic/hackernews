import React from 'react';
import { StoriesContainer } from './containers/storiesContainer';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { CommentsContainer } from './containers/commentsContainer';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navigation } from './components/navigation';
import './App.css';


export const App = () => (
    <Container>
        <Router>
            <Navigation/>
            <Switch>
                <Route path="/comments/:storyId">
                    <CommentsContainer />
                </Route>
                <Route path="">
                    <StoriesContainer />
                </Route>
            </Switch>
        </Router>
    </Container>
);