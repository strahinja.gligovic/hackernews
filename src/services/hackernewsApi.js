import axios from 'axios';
import { selectStoryFields, selectCommentFields } from '../selectors/selectFields';

export const baseUrl = 'https://hacker-news.firebaseio.com/v0/';
export const topStoriesUrl = `${baseUrl}topstories.json`;
export const itemUrl = `${baseUrl}item/`;

export const getStory = async (storyId) => {
    const result = await axios
        .get(`${itemUrl + storyId}.json`);

    return selectStoryFields(result.data);
}

export const getComment = async (commentId) => {
    const result = await axios.get(`${itemUrl + commentId}.json`);

    return selectCommentFields(result.data);
}

export const getStoryIds = async () => {
    const result = await axios.get(topStoriesUrl);

    return result.data;
}
